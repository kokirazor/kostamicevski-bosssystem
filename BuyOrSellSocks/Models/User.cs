﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BuyOrSellSocks.Models
{
    public class User
    {
        
        public int Id { get; set; }
        [Required]
        [Display(Name = "User Name")]
        public string UserName { get; set; }
        [Required]
        [Display(Name = "Password")]
        public string PassWord { get; set; }
        public int RoleID { get; set; }
        public int MoneyBalance { get; set; }
        public int SockBalance { get; set; } 
    }
}