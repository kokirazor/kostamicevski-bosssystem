﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BuyOrSellSocks.Models
{
    public static class Users
    {
        public static List<User> users { get; set; }

        static Users()
        {
            users = new List<User>
            {
                new User()
                {
                    Id = 1,
                    UserName = "Admin",
                    PassWord = "TheBoss",
                    RoleID = 1,
                    MoneyBalance = 100,
                    SockBalance = 90
                },
                new User()
                {
                    Id = 2,
                    UserName = "Boss",
                    PassWord = "TheBoss",
                    RoleID = 2,
                    MoneyBalance = 180,
                    SockBalance = 890
                }
            };
        }

        public static void AddNewUser(string UserName, string PassWord, int roleId)
        {
            users.Add(new User() { Id = users.Max(x => x.Id), UserName = UserName, PassWord = PassWord,RoleID = roleId });
        }

        public static bool DeleteUser(int Id)
        {
            bool res = false;
            try
            {
                users.Remove((from x in users where x.Id == Id select x).Single());
                res = true;
            }
            catch
            {
                res = false;
            }
            return res;
        }

        public static void UpdateUser(int Id, string UserName, string PassWord)
        {
            var ModifyUser = (from x in users where x.Id == Id select x).Single();
            ModifyUser.UserName = UserName;
            ModifyUser.PassWord = PassWord;
        }

        public static User GetUser(int Id)
        {
            return (from x in users where x.Id == Id select x).FirstOrDefault();
        }

        public static List<User> GetNonAdminUsers()
        {
            return (from x in users where x.RoleID != 1 select x).ToList();
        }
    }
}