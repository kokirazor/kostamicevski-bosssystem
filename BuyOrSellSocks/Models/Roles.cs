﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BuyOrSellSocks.Models
{
    public static class Roles
    {
        public static List<Role> roles { get; set; }

        static Roles()
        {
            roles = new List<Role>
            {
                new Role()
                {
                    id = 1,
                    role = "Admin"
                },
                new Role()
                {
                    id = 2,
                    role = "Boss"
                }
            };
        }
    }
}