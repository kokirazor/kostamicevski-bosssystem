﻿using System;
using System.Threading;

namespace BuyOrSellSocks.Models
{
    public class UniversalSockExchange
    {
        // Singleton instance
        private readonly static UniversalSockExchange _instance = new UniversalSockExchange();

        private readonly object _updateStockPricesLock = new object();
        private readonly Random Random = new Random();

        //stock can go up or down by a percentage of this factor on each change
        public decimal RandomDecimal;

        private readonly TimeSpan _updateInterval = TimeSpan.FromHours(1);
        private readonly Timer _timer;

        public UniversalSockExchange()
        {
            RandomDecimal = NextDecimal(Random, 0, 2);
            _timer = new Timer(UpdateSockDecimal, null, _updateInterval, _updateInterval);
        }

        public static UniversalSockExchange Instance
        {
            get
            {
                return _instance;
            }
        }

        public Timer Timer
        {
            get { return _timer; }
        }

        private void UpdateSockDecimal(object state)
        {
            lock (_updateStockPricesLock)
            {
                RandomDecimal = NextDecimal(Random, 0, 2);
            }
        }

        public decimal NextDecimal(Random random, int minimum, int maximum)
        {
            return (decimal)(random.NextDouble() * (maximum - minimum) + minimum);
        }

    }
}