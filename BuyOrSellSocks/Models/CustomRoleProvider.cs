﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using BuyOrSellSocks.Models;

namespace BuyOrSellSocks.Models
{
    public class CustomRoleProvider : RoleProvider
    {

        public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config)
        {
            base.Initialize(name, config);
        }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override string ApplicationName
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }

        public override string[] GetAllRoles()
        {

            return Roles.roles.Select(x => x.role).ToArray<string>();
            //throw new NotImplementedException();
        }

        public override string[] GetRolesForUser(string username)
        {
            try
            {
                var user = (from x in Users.users where x.UserName == username select x).First();
                if (user.RoleID == 1)
                {
                    return new string[] { "Admin" };
                }
                if (user.RoleID == 2)
                {
                    return new string[] { "Boss" };
                }
                else return null;
            }
            catch
            {
                return null;
            }
            //throw new NotImplementedException();
        }

        public override string[] GetUsersInRole(string roleName)
        {
            bool isAdmin = false;
            if (roleName == "Admin")
                isAdmin = true;

            try
            {
                var users_array = new List<string>();
                var users = (from x in Users.users join y in Roles.roles on x.RoleID equals y.id select x).ToArray();
                for (int i = 0; i < users.Length; i++)
                {
                    users_array.Add(users[i].UserName);
                }
                return users_array.ToArray();
            }
            catch
            {
                return null;
            }
            //throw new NotImplementedException();
        }

        public override bool IsUserInRole(string username, string roleName)
        {
            bool isUserInRole = false;
            if (roleName == "Admin")
            {
                try
                {
                    var exists = (from x in Users.users
                                  join y in Roles.roles on x.RoleID equals y.id
                                  where
                                    x.UserName == username &&
                                    y.role == "Admin"
                                  select x).Count();
                    if (exists == 1)
                        isUserInRole = true;
                }
                catch
                {
                    return false;
                }
            }
            if (roleName == "Boss")
            {
                try
                {
                    var exists = (from x in Users.users join y in Roles.roles on x.RoleID equals y.id where x.UserName == username && y.role == "Boss" select x).Count();
                    if (exists == 1)
                        isUserInRole = true;
                }
                catch
                {
                    return false;
                }
            }
            return isUserInRole;
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            if (roleName == "Admin" || roleName == "Boss")
                return true;
            else
                return false;
            //throw new NotImplementedException();
        }
    }
}