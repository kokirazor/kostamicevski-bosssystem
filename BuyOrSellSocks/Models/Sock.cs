﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BuyOrSellSocks.Models
{
    public class Sock
    {
        public int Id {get; set;}
        public string Name {get; set;}
        public string Category {get; set;}
        public decimal Price {
            get{
                return Price * UniversalSockExchange.Instance.RandomDecimal;
            } 
            set;
        }
    }
}