﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using BuyOrSellSocks.Models;

namespace BuyOrSellSocks.Controllers
{
    public class AdminController : ApiController
    {
        //
        // GET: /Admin/
        
        [Authorize(Roles = "Admin")]
        [AcceptVerbs("GET")]
        public List<User> GetUsers()
        {
            return Users.GetNonAdminUsers();
        }

        [Authorize(Roles = "Admin")]
        [AcceptVerbs("POST")]
        public HttpResponseMessage DeleteUser(int id)
        {
            return Request.CreateResponse(HttpStatusCode.OK, Users.DeleteUser(id));
        }

    }
}
