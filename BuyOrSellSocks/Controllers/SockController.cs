﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BuyOrSellSocks.Models;
namespace BuyOrSellSocks.Controllers
{
    public class SockController : Controller
    {
        //
        // GET: /Sock/

        public static Sock[] _Sock = new[] 
        { 
            new Sock { Id = 1, Name = "Sock1", Category = "Category1", Price = 1 }, 
            new Sock { Id = 2, Name = "Sock2", Category = "Category2", Price = 3.75M }, 
            new Sock { Id = 3, Name = "Sock3", Category = "Category3", Price = 16.99M } 
        };

        public IEnumerable<Sock> GetAllSocks()
        {
            return _Sock;
        }

    }
}
