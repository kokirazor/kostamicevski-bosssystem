﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BuyOrSellSocks.Models;

namespace BuyOrSellSocks.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        [Authorize(Roles = "Admin")]
        public ActionResult AdminPage()
        {

            return View(Users.GetNonAdminUsers());
        }
        [Authorize(Roles = "Boss")]
        public ActionResult BossPage()
        {
            return View();
        }
    }
}
