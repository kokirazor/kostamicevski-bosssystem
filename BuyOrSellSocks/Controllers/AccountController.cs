﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using BuyOrSellSocks.Models;
using System.Web.Routing;
using System.IO;

namespace BuyOrSellSocks.Controllers
{
    public class AccountController : Controller
    {

        public IFormsAuthenticationService FormsService { get; set; }
        public IMembershipService MembershipService { get; set; }
        protected override void Initialize(RequestContext requestContext)
        {

            if (FormsService == null) { FormsService = new FormsAuthenticationService(); }
            if (MembershipService == null) { MembershipService = new AccountMembershipService(); }

            base.Initialize(requestContext);
        }

        [AllowAnonymous]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Login(string returnUrl)
        {
                ViewBag.ReturnUrl = returnUrl;
                return View();
        }


        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public ActionResult Login(User model)
        {
            if (MembershipService.ValidateUser(model.UserName, model.PassWord))
            {
                var user = (from x in Users.users where x.UserName == model.UserName select x).FirstOrDefault();

                if (user == null)
                {
                    ModelState.Clear();
                    ModelState.AddModelError("", ("Error!"));
                    return View(model);
                }
                FormsService.SignIn(model.UserName, true);

                CustomRoleProvider userRole = new CustomRoleProvider();
                if (userRole.IsUserInRole(model.UserName, "Admin"))
                {
                    return Redirect("~/Home/AdminPage");
                }
                if (userRole.IsUserInRole(model.UserName, "Boss"))
                {
                    return Redirect("~/Home/BossPage");
                }
            }
            else
            {
                ModelState.Clear();
                ModelState.AddModelError("", ("Error!"));
            }
            return View(model);
        }




        // POST: /Account/LogOff
        [HttpPost]
        public ActionResult LogOff()
        {
            //WebSecurity.Logout();
            FormsService.SignOut();
            return RedirectToAction("Login", "Account");
        }


        

        //[Authorize(Roles = "SuperAdmin")]
        //public ActionResult EditAdmin(int id)
        //{
        //    var admin = db.Administrator.FirstOrDefault(x => x.id == id);
        //    EditAdmin edit_admin = new EditAdmin()
        //    {
        //        ID = id,
        //        Mail = admin.Email,
        //        Name = admin.UserName
        //    };
        //    return View(edit_admin);
        //}

        //[Authorize(Roles = "SuperAdmin")]
        //public ActionResult SaveAdmin(EditAdmin admin_model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        try
        //        {
        //            var admin = db.Administrator.FirstOrDefault(x => x.id == admin_model.ID);
        //            if (admin != null)
        //            {
        //                System.Security.Cryptography.MD5CryptoServiceProvider md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
        //                byte[] bs = System.Text.Encoding.UTF8.GetBytes(admin_model.NewPassword);
        //                bs = md5.ComputeHash(bs);

        //                admin.Email = admin_model.Mail;
        //                admin.UserName = admin_model.Name;
        //                admin.PassWord = bs;

        //                db.SaveChanges();
        //            }

        //            return RedirectToAction("Login", "Account");
        //        }
        //        catch (Exception ex)
        //        {
        //            return View("EditAdmin", admin_model);
        //        }
        //    }
        //    return View("EditAdmin", admin_model);
        //}

        //
        // GET: /Account/Register
        
        


    }
}